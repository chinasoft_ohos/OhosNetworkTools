package com.stealthcotper.ohosnetworktools;

import com.stealthcopter.networktools.ARPInfo;
import com.stealthcopter.networktools.IPTools;
import com.stealthcopter.networktools.PortScan;
import com.stealthcopter.networktools.SubnetDevices;
import com.stealthcopter.networktools.subnet.Device;
import org.junit.Test;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;

import static org.junit.Assert.assertEquals;

public class ExampleOhosTest {
    @Test
    public void testIpAddres() {
        InetAddress ipAddress = IPTools.getLocalIpv4Address();
        String hostAddress = ipAddress.getHostAddress();
        assertEquals(hostAddress,"10.0.5.2"); // 模拟器的ip地址
    }
    @Test
    public void testIpMAC() {
        // 模拟器的ip
        String macAddress = ARPInfo.getMACFromIPAddress("10.0.5.1");
        assertEquals(macAddress,"02:00:00:00:01:00");
    }
    @Test
    public void testPost(){
        try {
            PortScan.onAddress("10.0.5.2").setPortsAll().setMethodTcP().doScan(new PortScan.PortListener() {
                @Override
                public void onResult(int portNo, boolean isOpen) {
                }

                @Override
                public void onFinished(ArrayList<Integer> openPorts) {
                }
            });
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testSubnet(){
        SubnetDevices.fromLocalAddress().findDevices(new SubnetDevices.OnSubnetDeviceFound() {
            @Override
            public void onDeviceFound(Device device) {
               assertEquals(device.ip,"10.0.5.2");
            }

            @Override
            public void onFinished(ArrayList<Device> devicesFound) {
            }
        });
    }
}