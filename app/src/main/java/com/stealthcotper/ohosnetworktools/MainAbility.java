package com.stealthcotper.ohosnetworktools;

import com.stealthcopter.networktools.*;
import com.stealthcopter.networktools.ping.PingResult;
import com.stealthcopter.networktools.ping.PingStats;
import com.stealthcopter.networktools.subnet.Device;
import com.stealthcotper.ohosnetworktools.btnview.Ripple;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorProperty;
import ohos.agp.components.*;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.render.Canvas;
import ohos.agp.utils.Color;
import ohos.agp.utils.TextTool;
import ohos.agp.window.service.WindowManager;
import ohos.app.dispatcher.Group;
import ohos.app.dispatcher.TaskDispatcher;
import ohos.app.dispatcher.task.TaskPriority;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import ohos.utils.IntentConstants;
import ohos.utils.net.Uri;

import java.io.IOException;
import java.net.*;
import java.util.ArrayList;
import java.util.Locale;

public class MainAbility extends Ability implements Component.ClickedListener,
        Component.FocusChangedListener, Animator.StateChangedListener {
    private Ripple pingButton;
    private Ripple wolButton;
    private Ripple portScanButton;
    private Ripple subnetDevicesButton;
    private Text mEditHint;
    private Text resultText;
    private Text mEditAnim;
    private TextField editIpAddress;
    private ScrollView scrollView;
    private TaskDispatcher parallelTaskDispatcher;
    private Group group;
    private EventHandler mHandler;
    private int mTitleColor = -1;
    private int size;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        WindowManager.getInstance().getTopWindow().get().setStatusBarColor(mTitleColor);
        super.setUIContent(ResourceTable.Layout_ability_main);
        initView();
    }

    private void initView() {
        mEditAnim = (Text) findComponentById(ResourceTable.Id_edit_anim);
        mEditHint = (Text) findComponentById(ResourceTable.Id_edit_hint);
        pingButton = (Ripple) findComponentById(ResourceTable.Id_pingButton);
        wolButton = (Ripple) findComponentById(ResourceTable.Id_wolButton);
        portScanButton = (Ripple) findComponentById(ResourceTable.Id_portScanButton);
        subnetDevicesButton = (Ripple) findComponentById(ResourceTable.Id_subnetDevicesButton);
        resultText = (Text) findComponentById(ResourceTable.Id_resultText);
        editIpAddress = (TextField) findComponentById(ResourceTable.Id_editIpAddress);
        scrollView = (ScrollView) findComponentById(ResourceTable.Id_scrollView1);
        findComponentById(ResourceTable.Id_uri_github).setClickedListener(this);
        pingButton.setClickedListener(this);
        wolButton.setClickedListener(this);
        portScanButton.setClickedListener(this);
        subnetDevicesButton.setClickedListener(this);
        editIpAddress.setFocusChangedListener(this);
        editIpAddress.setMaxTextLines(1);
        address();
        // 线程，Hnadler
        mHandler = new EventHandler(EventRunner.getMainEventRunner());
        String dispatcherName = "serialTaskDispatcher";
        parallelTaskDispatcher = createParallelTaskDispatcher(dispatcherName, TaskPriority.DEFAULT);
        group = parallelTaskDispatcher.createDispatchGroup();
    }

    private void address() {
        // 获取Ipv4，需添加网络权限
        InetAddress ipAddress = IPTools.getLocalIpv4Address();
        if (ipAddress != null) {
            editIpAddress.setText(ipAddress.getHostAddress());
            mEditHint.setVisibility(Component.VISIBLE);
            mEditAnim.setVisibility(Component.VERTICAL);
        } else {
            mEditAnim.setVisibility(Component.VISIBLE);
            mEditHint.setVisibility(Component.VERTICAL);
        }
    }

    @Override
    public void onClick(Component component) {
        switch (component.getId()) {
            case ResourceTable.Id_pingButton:
                parallelTaskDispatcher.asyncGroupDispatch(group, new Runnable() {
                    @Override
                    public void run() {
                        doPing();
                    }
                });
                break;
            case ResourceTable.Id_wolButton:
                parallelTaskDispatcher.asyncGroupDispatch(group, new Runnable() {
                    @Override
                    public void run() {
                        doWakeOnLan();
                    }
                });
                break;
            case ResourceTable.Id_portScanButton:
                parallelTaskDispatcher.asyncGroupDispatch(group, new Runnable() {
                    @Override
                    public void run() {
                        try {
                            doPortScan();
                        } catch (UnknownHostException e) {
                            e.printStackTrace();
                        }
                    }
                });
                break;
            case ResourceTable.Id_subnetDevicesButton:
                parallelTaskDispatcher.asyncGroupDispatch(group, new Runnable() {
                    @Override
                    public void run() {
                        findSubnetDevices();
                    }
                });
                break;
            case ResourceTable.Id_uri_github:
                Intent intents = new Intent();
                Operation operation = new Intent.OperationBuilder()
                        .withAction(IntentConstants.ACTION_SEARCH)
                        .withUri(Uri.parse("https://github.com/stealthcopter/AndroidNetworkTools"))
                        .build();
                intents.setOperation(operation);
                startAbility(intents);
                break;
            default:
        }
    }

    private void findSubnetDevices() {
        setEnabled(subnetDevicesButton, false);

        final long startTimeMillis = System.currentTimeMillis();

        SubnetDevices.fromLocalAddress().findDevices(new SubnetDevices.OnSubnetDeviceFound() {
            @Override
            public void onDeviceFound(Device device) {
                appendResultsText("Device: " + device.ip + " " + device.hostname);
            }

            @Override
            public void onFinished(ArrayList<Device> devicesFound) {
                float timeTaken = (System.currentTimeMillis() - startTimeMillis) / 1000.0f;
                appendResultsText("Devices Found: " + devicesFound.size());
                appendResultsText("Finished " + timeTaken + " s");
                setEnabled(subnetDevicesButton, true);
            }
        });
    }

    private void doPortScan() throws UnknownHostException {
        String ipAddress = editIpAddress.getText();
        if (TextTool.isNullOrEmpty(ipAddress)) {
            appendResultsText("Invalid Ip Address");
            return;
        }

        setEnabled(portScanButton, false);

        appendResultsText("PortScanning IP: " + ipAddress);
        PortScan.onAddress(ipAddress).setPort(21).setMethodTcP().doScan();

        final long startTimeMillis = System.currentTimeMillis();

        PortScan.onAddress(ipAddress).setPortsAll().setMethodTcP().doScan(new PortScan.PortListener() {
            @Override
            public void onResult(int portNo, boolean isOpen) {
                if (isOpen) {
                    appendResultsText("Open: " + portNo);
                }
            }

            @Override
            public void onFinished(ArrayList<Integer> openPorts) {
                appendResultsText("Open Ports: " + openPorts.size());
                appendResultsText("Time Taken: " + ((System.currentTimeMillis() - startTimeMillis) / 1000.0f));
                setEnabled(portScanButton, true);
            }
        });
    }

    private void doWakeOnLan() {
        String ipAddress = editIpAddress.getText();
        if (TextTool.isNullOrEmpty(ipAddress)) {
            appendResultsText("Invalid Ip Address");
            return;
        }
        setEnabled(wolButton, false);

        appendResultsText("IP address: " + ipAddress);

        String macAddress = ARPInfo.getMACFromIPAddress(ipAddress);

        if (macAddress == null) {
            appendResultsText("Could not fromIPAddress MAC address, cannot send WOL packet without it.");
            setEnabled(wolButton, true);
            return;
        }

        appendResultsText("MAC address: " + macAddress);
        if (macAddress.matches("..:..:..:..:..:..")) {
            appendResultsText("IP address2: " + ARPInfo.getIPAddressFromMAC(macAddress));

            try {
                WakeOnLan.sendWakeOnLan(ipAddress, macAddress);
                appendResultsText("WOL Packet sent");
            } catch (IOException e) {
                appendResultsText(e.getMessage());
                e.printStackTrace();
            } finally {
                setEnabled(wolButton, true);
            }
        }
    }

    private void doPing() {
        String ipAddress = editIpAddress.getText();
        if (TextTool.isNullOrEmpty(ipAddress)) {
            appendResultsText("Invalid Ip Address");
            return;
        }
        setEnabled(pingButton, false);
        PingResult pingResult = null;
        try {
            pingResult = Ping.onAddress(ipAddress).setTimeOutMillis(1000).doPing();
        } catch (UnknownHostException e) {
            e.printStackTrace();
            appendResultsText(e.getMessage());
            setEnabled(pingButton, true);
            return;
        }
        appendResultsText("Pinging Address: " + pingResult.getAddress().getHostAddress());
        appendResultsText("HostName: " + pingResult.getAddress().getHostName());
        appendResultsText(String.format("%.2f ms", pingResult.getTimeTaken()));

        // Perform an asynchronous ping
        Ping.onAddress(ipAddress).setTimeOutMillis(1000).setTimes(5).doPing(new Ping.PingListener() {
            @Override
            public void onResult(PingResult pingResult) {
                if (pingResult.isReachable) {
                    appendResultsText(String.format(Locale.ROOT,"%.2f ms", pingResult.getTimeTaken()));
                } else {
                    appendResultsText(getString(ResourceTable.String_timeout));
                }
            }

            @Override
            public void onFinished(PingStats pingStats) {
                appendResultsText(String.format(Locale.ROOT,"Pings: %d, Packets lost: %d",
                        pingStats.getNoPings(), pingStats.getPacketsLost()));
                appendResultsText(String.format(Locale.ROOT,"Min/Avg/Max Time: %.2f/%.2f/%.2f ms",
                        pingStats.getMinTimeTaken(), pingStats.getAverageTimeTaken(), pingStats.getMaxTimeTaken()));
                setEnabled(pingButton, true);
            }

            @Override
            public void onError(Exception e) {
                setEnabled(pingButton, true);
            }
        });
    }

    private void setEnabled(Ripple button, boolean isEnabled) {
        mHandler.postTask(new Runnable() {
            @Override
            public void run() {
                if (button != null) {
                    button.setEnabled(isEnabled);
                    button.setTextColor(new Color(Color.getIntColor("#212121")));
                    if (!isEnabled) {
                        button.setTextColor(new Color(Color.getIntColor("#A4A4A4")));
                    }
                }
            }
        });
    }

    private void appendResultsText(String text) {
        mHandler.postSyncTask(new Runnable() {
            @Override
            public void run() {
                resultText.append(text + "\n");
                scrollView.addDrawTask(new Component.DrawTask() {
                    @Override
                    public void onDraw(Component component, Canvas canvas) {
                        scrollView.fluentScrollYTo(resultText.getHeight());
                    }
                });
            }
        });
    }

    @Override
    public void onFocusChange(Component component, boolean isSelect) {
        AnimatorProperty animator = mEditAnim.createAnimatorProperty();
        animator.setStateChangedListener(MainAbility.this);
        if (isSelect) {
            // 选中改变基线
            ShapeElement errorElement = new ShapeElement(this, ResourceTable.Graphic_background_textfieid_basement);
            editIpAddress.setBasement(errorElement);
            mEditHint.setHintColor(new Color(Color.getIntColor("#8BC34A")));
            // 动画处理
            if (mEditAnim.getVisibility() == Component.VISIBLE) {
                animator.moveByY(-90).setCurveType(Animator.CurveType.LINEAR).setDuration(50);
                mEditAnim.setHintColor(new Color(Color.getIntColor("#8BC34A")));
                size = 13;
                animator.start();
            }
        } else {
            animator.moveByY(90).setCurveType(Animator.CurveType.LINEAR).setDuration(50);
            size = 20;
            animator.start();
        }
    }

    @Override
    public void onStart(Animator animator) {
        mEditAnim.setTextSize(size, Text.TextSizeType.FP);
    }

    @Override
    public void onStop(Animator animator) {
        animator.stop();
    }

    @Override
    public void onCancel(Animator animator) {
    }

    @Override
    public void onEnd(Animator animator) {
    }

    @Override
    public void onPause(Animator animator) {
    }

    @Override
    public void onResume(Animator animator) {
    }
}
