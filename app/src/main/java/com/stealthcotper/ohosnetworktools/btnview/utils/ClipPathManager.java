package com.stealthcotper.ohosnetworktools.btnview.utils;

import ohos.agp.render.Path;
/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

public class ClipPathManager {
    protected final Path path = new Path();

    /**
     * 构造
     */
    public ClipPathManager() {
    }

    /**
     * 设置裁剪布局
     *
     * @param width 宽度
     * @param height 高度
     */
    public void setupClipLayout(int width, int height) {
        path.reset();
        Path clipPath = null;
        if (createClipPath != null) {
            clipPath = createClipPath.createClipPath(width, height);
        }
        if (clipPath != null) {
            path.set(clipPath);
        }
    }

    /**
     * 设置裁剪路径
     *
     * @return 路径
     */
    public Path getClipPath() {
        return path;
    }

    /**
     * 创建Path接口
     */
    private ClipPathCreator createClipPath = null;

    /**
     * 设置剪裁
     *
     * @param createClipPath 剪裁路径
     */
    public void setClipPathCreator(ClipPathCreator createClipPath) {
        this.createClipPath = createClipPath;
    }

    /**
     * 接口
     */
    public interface ClipPathCreator {
        /**
         * 路径
         *
         * @param width 宽度
         * @param height 高度
         * @return 路径
         */
        Path createClipPath(int width, int height);
    }
}
