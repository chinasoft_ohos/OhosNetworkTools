package com.stealthcotper.ohosnetworktools.btnview;

import com.stealthcotper.ohosnetworktools.btnview.inface.RHelper;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Text;
import ohos.app.Context;
/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

public class Ripple extends Text implements RHelper<RippleHelper> {
    private RippleHelper mHelper;

    public Ripple(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);

    }

    public Ripple(Context context) {
        this(context, null);

    }

    public Ripple(Context context, AttrSet attrs) {
        super(context, attrs);

        mHelper = new RippleHelper(context, this, attrs);

    }

    @Override
    public RippleHelper getHelper() {
        return mHelper;
    }

    @Override
    public boolean isTextCursorVisible() {
        return super.isTextCursorVisible();
    }
}
