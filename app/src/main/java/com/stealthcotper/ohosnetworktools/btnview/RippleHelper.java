package com.stealthcotper.ohosnetworktools.btnview;

import com.stealthcotper.ohosnetworktools.btnview.help.RBaseHelper;
import com.stealthcotper.ohosnetworktools.btnview.utils.AttrUtils;
import com.stealthcotper.ohosnetworktools.btnview.utils.TypedAttrUtils;
import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.Text;
import ohos.agp.components.element.Element;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.utils.Color;
import ohos.app.Context;
import ohos.multimodalinput.event.MmiPoint;
import ohos.multimodalinput.event.TouchEvent;
/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

public class RippleHelper extends RBaseHelper<Text> implements Component.TouchEventListener, Component.DrawTask {
    AnimatorValue animatorValue = new AnimatorValue();
    private int sy;
    private float sx;
    private float zj;
    private Paint paint;
    private Color mTextRippleColor;
    private boolean isRipple;

    /**
     * 构造
     *
     * @param context 上下文
     * @param text 文本
     * @param attrs 属性集
     */
    public RippleHelper(Context context, Text text, AttrSet attrs) {
        super(context, text, attrs);
        mView.setTouchEventListener(this::onTouchEvent);
        mView.addDrawTask(this);
        mView.setTouchEventListener(this);
        initAttributeSet(context, attrs);
        paint = new Paint();
        paint.setColor(mTextRippleColor);
        paint.setAlpha(0.2f);
        paint.setStyle(Paint.Style.FILL_STYLE);
        animatorValue.setDuration(500);
        animatorValue.setCurveType(Animator.CurveType.INVALID);
    }

    /**
     * 初始化控件属性
     *
     * @param context 上下集
     * @param attrs 属性集
     */
    private void initAttributeSet(Context context, AttrSet attrs) {
        if (context == null || attrs == null) {
            return;
        }
        mTextRippleColor = AttrUtils.getStringToColor(attrs, "text_ripple_color", Color.TRANSPARENT);
        isRipple = AttrUtils.getStringToBool(attrs, "text_ripple", false);
    }

    /**
     * touch事件
     *
     * @param component component类
     * @param event touch事件
     * @return 是否拦截
     */
    @Override
    public boolean onTouchEvent(Component component, TouchEvent event) {
        if (isRipple && mView.isEnabled()) {
            sx = (float) (component.getWidth() / 2d);
            sy = component.getHeight() / 2;
            animatorValue.start();
        }
        return false;
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        if (isRipple && mView.isEnabled()) {
            animatorValue.setValueUpdateListener(new AnimatorValue.ValueUpdateListener() {
                @Override
                public void onUpdate(AnimatorValue animatorValue, float v) {
                    zj = v;
                    if (v >= 1.0) {
                        paint.setAlpha(0f);
                    } else {
                        paint.setAlpha(0.2f);
                    }
                    mView.invalidate();
                }
            });
            if (mView.getWidth() > mView.getHeight()) {
                canvas.drawCircle(sx, sy, mView.getWidth() * zj, paint);
            } else {
                canvas.drawCircle(sx, sy, mView.getHeight() * zj, paint);
            }
        }
    }
}
