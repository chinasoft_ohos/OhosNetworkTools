package com.stealthcotper.ohosnetworktools.btnview.help;

import com.stealthcotper.ohosnetworktools.btnview.inface.Film;
import com.stealthcotper.ohosnetworktools.btnview.utils.AttrUtils;
import com.stealthcotper.ohosnetworktools.btnview.utils.DensityUtils;
import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.element.Element;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.components.element.StateElement;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.utils.Color;
import ohos.app.Context;
import ohos.multimodalinput.event.MmiPoint;

import static ohos.agp.components.ComponentState.COMPONENT_STATE_EMPTY;

/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

public class RBaseHelper<T extends Component> implements Film {
    protected int bgColors = 1;
    protected Context mContext;
    protected T mView;
    protected FilmHelper mClipHelper = new FilmHelper();
    AnimatorValue animatorValue = new AnimatorValue();
    private String mBackgroundNormals = "background_normal";
    private String mCornerGarden = "corner_radius";
    private String mGradientTypes = "gradient_type";
    private String mGradientRarden = "gradient_radius";

    private float mCornerRadius;
    private Color mBackgroundColorNormal;

    private ShapeElement mBackgroundNormal;
    private int mGradientType = 0;
    private float mGradientRadius;

    private Color mRippleColor;

    private Element mViewBackground;
    private Element mBackgroundDrawable;

    private int[][] states = new int[6][];
    private StateElement mStateBackground;
    private float[] mBorderRadii = new float[8];
    private Paint paint;

    public RBaseHelper(Context context, T view, AttrSet attrs) {
        mView = view;
        mContext = context;
        initAttributeSet(context, attrs);
        paint = new Paint();
        paint.setColor(mRippleColor);
        paint.setAlpha(0.2f);
        paint.setStyle(Paint.Style.FILL_STYLE);
        animatorValue.setDuration(1000);
        animatorValue.setCurveType(Animator.CurveType.LINEAR);
    }

    /**
     * 初始化控件属性
     *
     * @param context
     * @param attrs
     */
    private void initAttributeSet(Context context, AttrSet attrs) {
        if (context == null || attrs == null) {
            return;
        }
        mCornerRadius = AttrUtils.getInt(attrs, mCornerGarden, -1);
        Object[] bgInfoNormal;
        bgInfoNormal = getBackgroundInfo(attrs, mBackgroundNormals);
        mBackgroundColorNormal = (Color) bgInfoNormal[1];
        // gradient
        mGradientType = AttrUtils.getInt(attrs, mGradientTypes, 0);
        mGradientRadius = AttrUtils.getFloat(attrs, mGradientRarden, -1);
        mRippleColor = getStringColor(attrs, "ripple_color", null);
        setup();
    }

    private Color getStringColor(AttrSet attrs, String colorKey, String s) {
        String color = AttrUtils.getString(attrs, colorKey, s);
        if (color == null) {
            return Color.TRANSPARENT;
        }
        return new Color(Color.getIntColor(color));
    }

    /**
     * 设置
     */
    private void setup() {
        mBackgroundNormal = new ShapeElement();
        mViewBackground = mView.getBackgroundElement();
        mStateBackground = new StateElement();

        mBackgroundNormal.setRgbColor(RgbColor.fromArgbInt(mBackgroundColorNormal.getValue()));
        setGradient();
        states[0] = new int[]{COMPONENT_STATE_EMPTY};
        mStateBackground.addState(states[0], mBackgroundNormal);

        // 设置圆角
        setRadiusValue();
    }

    private void setGradient() {
        mBackgroundNormal.setShaderType(mGradientType);
        mBackgroundNormal.setCornerRadius(mGradientRadius);
    }

    private void setBackgroundState() {
        boolean isHasCustom = false;
        boolean isHasCusBg;
        boolean isHasCusBorder = false;
        boolean isHasCusCorner = false;
        boolean isUnHasBgColor = mBackgroundColorNormal == null;
        if (isUnHasBgColor) {
            isHasCusBg = false;
        } else {
            isHasCusBg = true;
        }

        if (isHasCusBg || isHasCusCorner || isHasCusBorder) {
            isHasCustom = true;
        }

        if (!isHasCustom) {
            mBackgroundDrawable = mViewBackground; // 使用原生背景
        } else {
            mBackgroundDrawable = getBackgroundDrawable();
        }
        mView.setBackground(mBackgroundDrawable);
    }

    private Element getBackgroundDrawable() {
        return mStateBackground;
    }

    private void setRadiusUi() {
        mBackgroundNormal.setCornerRadiiArray(mBorderRadii);
        setBackgroundState();
    }

    /**
     * 设置圆角数值
     */
    private void setRadiusValue() {
        if (mCornerRadius >= 0) {
            mBorderRadii[0] = DensityUtils.pxToFp(mContext, mCornerRadius);
            mBorderRadii[1] = DensityUtils.pxToFp(mContext, mCornerRadius);
            mBorderRadii[2] = DensityUtils.pxToFp(mContext, mCornerRadius);
            mBorderRadii[3] = DensityUtils.pxToFp(mContext, mCornerRadius);
            mBorderRadii[4] = DensityUtils.pxToFp(mContext, mCornerRadius);
            mBorderRadii[5] = DensityUtils.pxToFp(mContext, mCornerRadius);
            mBorderRadii[6] = DensityUtils.pxToFp(mContext, mCornerRadius);
            mBorderRadii[7] = DensityUtils.pxToFp(mContext, mCornerRadius);
            setRadiusUi();
            return;
        }
    }

    private Object[] getBackgroundInfo(AttrSet attr, String key) {
        Color[] colors = null;

        int bgType = bgColors; // 背景类型
        Color bgColor = null; // 单一颜色
        Element bgElement = null;
        bgType = bgColors;
        bgColor = getStringColor(attr, key, "#ffffff");
        return new Object[]{bgType, bgColor, colors, bgElement};
    }

    @Override
    public void dispatchDraw(Canvas canvas) {
        mClipHelper.dispatchDraw(canvas);
    }

    @Override
    public void onLayout(int left, int top, int right, int bottom) {
        mClipHelper.onLayout(left, top, right, bottom);
    }
}
