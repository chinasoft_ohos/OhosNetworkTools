# OhosNetworkTools

## 项目介绍
- 项目名称：OhosNetworkTools
- 所属系列：openharmony的第三方组件适配移植
- 功能：这是一个 networkTools网络工具类，端口扫描，子网设备查找（本地网络上发现设备）
- 项目移植状态：主功能完成
- 调用差异：无
- 开发版本：sdk6，DevEco Studio 2.2 Beta1
- 基线版本：Release 0.4.5.3

## 效果演示
![输入图片说明](https://gitee.com/chinasoft_ohos/OhosNetworkTools/raw/feature/develop/img/IPq.gif "IPq.gif")

## 安装教程
1.在项目根目录下的build.gradle文件中
```
allprojects {
   repositories {
       maven {
           url 'https://s01.oss.sonatype.org/content/repositories/releases/'
       }
   }
}
```
2.在entry模块的build.gradle文件中
```
dependencies {
   implementation('com.gitee.chinasoft_ohos:OhosNetworkTools:1.0.0')
   ......
}
```
在sdk6，DevEco Studio 2.2 Beta1下项目可直接运行 如无法运行，删除项目.gradle,.idea,build,gradle,build.gradle文件, 并依据自己的版本创建新项目，将新项目的对应文件复制到根目录下

## 使用说明
1、这是一个 networkTools网络工具类，端口扫描，子网设备查找（本地网络上发现设备）

```
获取IP,查询连接
IPTools.getLocalIPv4Address();

Ping.onAddress(ipAddress).setTimeOutMillis(1000).setTimes(5).doPing(new Ping.PingListener() {
            @Override
            public void onResult(PingResult pingResult) {
                if (pingResult.isReachable) {
                    appendResultsText(String.format("%.2f ms", pingResult.getTimeTaken()));
                } else {
                    appendResultsText(getString(ResourceTable.String_timeout));
                }
            }

            @Override
            public void onFinished(PingStats pingStats) {
                appendResultsText(String.format("Pings: %d, Packets lost: %d",
                        pingStats.getNoPings(), pingStats.getPacketsLost()));
                appendResultsText(String.format("Min/Avg/Max Time: %.2f/%.2f/%.2f ms",
                        pingStats.getMinTimeTaken(), pingStats.getAverageTimeTaken(), pingStats.getMaxTimeTaken()));
                setEnabled(pingButton, true);
            }

            @Override
            public void onError(Exception e) {
                setEnabled(pingButton, true);
            }
        });
```
```
端口扫描
  PortScan portScan = PortScan.onAddress(ipAddress).setPortsAll().setMethodTcP().doScan(new PortScan.PortListener() {
            @Override
            public void onResult(int portNo, boolean open) {
                if (open) appendResultsText("Open: " + portNo);
            }

            @Override
            public void onFinished(ArrayList<Integer> openPorts) {
                appendResultsText("Open Ports: " + openPorts.size());
                appendResultsText("Time Taken: " + ((System.currentTimeMillis() - startTimeMillis) / 1000.0f));
                setEnabled(portScanButton, true);
            }
        }); 详情见测试用例

```
## 测试信息
codeCheck代码测试无异常

CloudTest代码测试无异常

病毒安全检测通过

当前版本demo功能与原组件基本无差异

## 版本迭代
- 1.0.0

## 版权和许可信息
- Apache 2.0
