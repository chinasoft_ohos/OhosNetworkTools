package com.stealthcopter.networktools.subnet;

import java.net.InetAddress;

public class Device {
    /**
     * ip
     */
    public String ip;

    /**
     * host
     */
    public String hostname;

    /**
     * mac
     */
    public String mac;

    /**
     * 时间
     */
    public float time = 0;

    /**
     * 构造
     *
     * @param ip 地址
     */
    public Device(InetAddress ip) {
        this.ip = ip.getHostAddress();
        this.hostname = ip.getCanonicalHostName();
    }

    @Override
    public String toString() {
        return "Device{"
                + "ip='" + ip + '\''
                + ", hostname='" + hostname + '\''
                + ", mac='" + mac + '\''
                + ", time=" + time
                + '}';
    }
}

