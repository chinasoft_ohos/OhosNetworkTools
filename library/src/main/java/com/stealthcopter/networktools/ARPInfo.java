package com.stealthcopter.networktools;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Looks at the file at /proc/net/arp to fromIPAddress ip/mac addresses from the cache
 * We assume that the file has this structure:
 * Also looks at the output from `ip sleigh show` command
 */
public class ARPInfo {
    private ARPInfo() {
    }

    /**
     * 尝试从给定的IP地址提取硬件MAC地址
     *
     * @param ip - IP
     * @return MAC
     */
    public static String getMACFromIPAddress(String ip) {
        if (ip == null) {
            return null;
        }

        HashMap<String, String> cache = getAllIPAndMACAddressesInARPCache();
        return cache.get(ip);
    }

    /**
     * 从给定的IP地址提取硬件MAC地址
     *
     * @param macAddress "01:23:45:67:89:ab"
     * @return IP
     */
    public static String getIPAddressFromMAC(String macAddress) {
        if (macAddress == null) {
            return null;
        }

        if (!macAddress.matches("..:..:..:..:..:..")) {
            throw new IllegalArgumentException("Invalid MAC Address");
        }

        HashMap<String, String> cache = getAllIPAndMACAddressesInARPCache();
        for (String ip : cache.keySet()) {
            if (cache.get(ip).equalsIgnoreCase(macAddress)) {
                return ip;
            }
        }
        return null;
    }

    /**
     * 返回当前ARP缓存中的所有ip地址(/proc/net/arp)。
     *
     * @return 发现的IP地址列表
     */
    public static ArrayList<String> getAllIPAddressesInARPCache() {
        return new ArrayList<>(getAllIPAndMACAddressesInARPCache().keySet());
    }

    /**
     * 返回当前ARP缓存中的所有MAC地址(/proc/net/arp)。
     *
     * @return 找到的MAC地址列表
     */
    public static ArrayList<String> getAllMACAddressesInARPCache() {
        return new ArrayList<>(getAllIPAndMACAddressesInARPCache().values());
    }

    /**
     * 返回当前在以下位置的所有IP/MAC地址对ARP缓存(/ proc / net/arp)。
     *
     * @return 发现的IP/MAC地址对的列表
     */
    public static HashMap<String, String> getAllIPAndMACAddressesInARPCache() {
        HashMap<String, String> macList = getAllIPandMACAddressesFromIPSleigh();
        for (String line : getLinesInARPCache()) {
            String[] splitted = line.split(" +");
            if (splitted.length >= 4) {
                // Ignore values with invalid MAC addresses
                if (splitted[3].matches("..:..:..:..:..:..")
                        && !splitted[3].equals("00:00:00:00:00:00")) {
                    if (!macList.containsKey(splitted[0])) {
                        macList.put(splitted[0], splitted[3]);
                    }
                }
            }
        }
        return macList;
    }

    private static ArrayList<String> getLinesInARPCache() {
        ArrayList<String> lines = new ArrayList<>();

        // If we cant read the file just return empty list
        if (!new File("/proc/net/arp").canRead()) {
            return lines;
        }

        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader("/proc/net/arp"));
            String line;
            while ((line = br.readLine()) != null) {
                lines.add(line);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (br != null) {
                    br.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return lines;
    }

    /**
     * 从' IP sleigh show '命令中获取IP / MAC地址对
     *
     * @return ip和mac地址的hashmap
     */
    public static HashMap<String, String> getAllIPandMACAddressesFromIPSleigh() {
        HashMap<String, String> macList = new HashMap<>();

        try {
            Runtime runtime = Runtime.getRuntime();
            Process proc = runtime.exec("ip neigh show");
            proc.waitFor();

            InputStreamReader reader = new InputStreamReader(proc.getInputStream());
            BufferedReader buffer = new BufferedReader(reader);
            String line;
            while ((line = buffer.readLine()) != null) {
                String[] splits = line.split(" ");
                if (splits.length < 4) {
                    continue;
                }
                macList.put(splits[0], splits[4]);
            }
        } catch (IOException | InterruptedException e) {
            e.fillInStackTrace();
        }

        return macList;
    }
}
