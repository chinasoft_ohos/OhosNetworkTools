package com.stealthcopter.networktools;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

/**
 * 测试了这个，它唤醒了我的电脑
 * <p>
 * Ref: http://www.jibble.org/wake-on-lan/
 */
public class WakeOnLan {
    /**
     * 默认 port
     */
    public static final int DEFAULT_PORT = 9;

    /**
     * 默认milis
     */
    public static final int DEFAULT_TIMEOUT_MILLIS = 10000;

    /**
     * 默认pack
     */
    public static final int DEFAULT_NO_PACKETS = 5;

    private String ipStr;
    private InetAddress inetAddress;
    private String macStr;
    private int port = DEFAULT_PORT;
    private int timeoutMillis = DEFAULT_TIMEOUT_MILLIS;
    private int noPackets = DEFAULT_NO_PACKETS;

    public interface WakeOnLanListener {
        /***
         * 成功
         */
        void onSuccess();

        /**
         * 错误
         *
         * @param e 错误信息
         */
        void onError(Exception e);
    }

    // 这个类不会被实例化
    private WakeOnLan() {
    }

    /**
     * 设置ip地址为wake
     *
     * @param ipStr - IP 地址待唤醒
     * @return 此对象允许链接
     */
    public static WakeOnLan onIp(String ipStr) {
        WakeOnLan wakeOnLan = new WakeOnLan();
        wakeOnLan.ipStr = ipStr;
        return wakeOnLan;
    }

    /**
     * 将地址设置为wake
     *
     * @param inetAddress - InetAddress需要被唤醒
     * @return 此对象允许链接
     */
    public static WakeOnLan onAddress(InetAddress inetAddress) {
        WakeOnLan wakeOnLan = new WakeOnLan();
        wakeOnLan.inetAddress = inetAddress;
        return wakeOnLan;
    }

    /**
     * 设置设备的mac地址为唤醒状态
     *
     * @param macStr - 被唤醒设备的MAC地址(必需的)
     * @return 此对象允许链接
     * @throws NullPointerException 异常
     */
    public WakeOnLan withMACAddress(String macStr) {
        if (macStr == null) {
            throw new NullPointerException("MAC Cannot be null");
        }
        this.macStr = macStr;
        return this;
    }

    /**
     * 设置发送数据包的端口，默认为9
     *
     * @param port - wol报文的端口
     * @return 此对象允许链接
     * @throws IllegalArgumentException 异常
     */
    public WakeOnLan setPort(int port) {
        if (port <= 0 || port > 65535) {
            throw new IllegalArgumentException("Invalid port " + port);
        }
        this.port = port;
        return this;
    }

    /**
     * 设置要发送的包的数量，这是为了克服网络的脆弱
     *
     * @param noPackets - 要发送的数据包数
     * @return 此对象允许链接
     * @throws IllegalArgumentException 异常
     */
    public WakeOnLan setNoPackets(int noPackets) {
        if (noPackets <= 0) {
            throw new IllegalArgumentException("Invalid number of packets to send " + noPackets);
        }
        this.noPackets = noPackets;
        return this;
    }

    /**
     * 设置套接字发送超时的毫秒数
     *
     * @param timeoutMillis - 超时
     * @return 此对象允许链接
     * @throws IllegalArgumentException 异常
     */
    public WakeOnLan setTimeout(int timeoutMillis) {
        if (timeoutMillis <= 0)
            throw new IllegalArgumentException("Timeout cannot be less than zero");
        this.timeoutMillis = timeoutMillis;
        return this;
    }

    /**
     * 同步调用wake方法。注意，这是一个网络请求，不应该是在UI线程执行
     *
     * @throws IOException - 从套接字抛出错误
     * @throws IllegalArgumentException 异常
     */
    public void wake() throws IOException {
        if (ipStr == null && inetAddress == null) {
            throw new IllegalArgumentException("You must declare ip address or supply an inetaddress");
        }

        if (macStr == null) {
            throw new NullPointerException("You did not supply a mac address with withMac(...)");
        }

        if (ipStr != null) {
            sendWakeOnLan(ipStr, macStr, port, timeoutMillis, noPackets);
        } else {
            sendWakeOnLan(inetAddress, macStr, port, timeoutMillis, noPackets);
        }
    }

    /**
     * 异步调用wake方法。这将在后台线程上执行并在完成或发生错误时触发监听器
     *
     * @param wakeOnLanListener - listener to call on result
     */
    public void wake(final WakeOnLanListener wakeOnLanListener) {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    wake();
                    if (wakeOnLanListener != null) {
                        wakeOnLanListener.onSuccess();
                    }
                } catch (IOException e) {
                    if (wakeOnLanListener != null) {
                        wakeOnLanListener.onError(e);
                    }
                }
            }
        });

        thread.start();
    }

    /**
     * 使用默认的10s超时时间向端口9发送唤醒局域网数据包
     *
     * @param ipStr -要发送到的IP字符串
     * @param macStr - 需要唤醒的MAC地址
     * @throws IllegalArgumentException - 无效的ip或mac
     * @throws IOException - 错误发送数据包
     */
    public static void sendWakeOnLan(String ipStr, String macStr) throws IllegalArgumentException, IOException {
        sendWakeOnLan(ipStr, macStr, DEFAULT_PORT, DEFAULT_TIMEOUT_MILLIS, DEFAULT_NO_PACKETS);
    }

    /**
     * 发送一个局域网唤醒包
     *
     * @param ipStr - 发送wol报文的IP字符串
     * @param macStr - 需要唤醒的MAC地址
     * @param port - 发送数据包
     * @param timeoutMillis - 超时
     * @param packets - 发送数据包的数量
     * @throws IllegalArgumentException - 无效的ip或mac
     * @throws IOException - 错误发送数据包
     */
    public static void sendWakeOnLan(final String ipStr, final String macStr, final int port, final int timeoutMillis,
                                     final int packets) throws IllegalArgumentException, IOException {
        if (ipStr == null) {
            throw new IllegalArgumentException("Address cannot be null");
        }
        InetAddress address = InetAddress.getByName(ipStr);
        sendWakeOnLan(address, macStr, port, timeoutMillis, packets);
    }

    /**
     * 发送一个局域网唤醒包
     *
     * @param address - 发送wol报文的InetAddress
     * @param macStr - 需要唤醒的MAC地址
     * @param port - 发送数据包到的端口
     * @param timeoutMillis - 超时
     * @param packets - 发送数据包的数量
     * @throws IllegalArgumentException - 无效的ip或mac
     * @throws IOException - 错误发送数据包
     */
    public static void sendWakeOnLan(final InetAddress address, final String macStr, final int port, final int timeoutMillis, final int packets) throws IllegalArgumentException, IOException {
        if (address == null) {
            throw new IllegalArgumentException("Address cannot be null");
        }
        if (macStr == null) {
            throw new IllegalArgumentException("MAC Address cannot be null");
        }
        if (port <= 0 || port > 65535) {
            throw new IllegalArgumentException("Invalid port " + port);
        }
        if (packets <= 0) {
            throw new IllegalArgumentException("Invalid number of packets to send " + packets);
        }
        byte[] macBytes = MACTools.getMacBytes(macStr);
        byte[] bytes = new byte[6 + 16 * macBytes.length];
        for (int i = 0; i < 6; i++) {
            bytes[i] = (byte) 0xff;
        }
        for (int i = 6; i < bytes.length; i += macBytes.length) {
            System.arraycopy(macBytes, 0, bytes, i, macBytes.length);
        }

        DatagramPacket packet = new DatagramPacket(bytes, bytes.length, address, port);

        // W在局域网上唤醒是不可靠的，所以最好发送数据包几次
        for (int i = 0; i < packets; i++) {
            DatagramSocket socket = new DatagramSocket();
            socket.setSoTimeout(timeoutMillis);
            socket.send(packet);
            socket.close();
        }
    }
}