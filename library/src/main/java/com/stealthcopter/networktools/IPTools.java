package com.stealthcopter.networktools;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.regex.Pattern;

public class IPTools {
    /**
     * ip匹配模式
     * https://examples.javacodegeeks.com/core-java/util/regex/regular-expressions-for-ip-v4-and-ip-v6-addresses/
     * 请注意，这些模式将匹配大多数但不是所有有效的ip
     */

    private static final Pattern IPV4_PATTERN =
            Pattern.compile(
                    "^(25[0-5]|2[0-4]\\d|[0-1]?\\d?\\d)(\\.(25[0-5]|2[0-4]\\d|[0-1]?\\d?\\d)){3}$");

    private static final Pattern IPV6_STD_PATTERN =
            Pattern.compile(
                    "^(?:[0-9a-fA-F]{1,4}:){7}[0-9a-fA-F]{1,4}$");

    private static final Pattern IPV6_HEX_COMPRESSED_PATTERN =
            Pattern.compile(
                    "^((?:[0-9A-Fa-f]{1,4}(?::[0-9A-Fa-f]{1,4})*)?)::((?:[0-9A-Fa-f]{1,4}(?::[0-9A-Fa-f]{1,4})*)?)$");

    private IPTools() {
    }

    /**
     * 是否是ipv4
     *
     * @param address ip
     * @return true 是，反之
     */
    public static boolean isIpv4Address(final String address) {
        return address != null && IPV4_PATTERN.matcher(address).matches();
    }

    /**
     * 是否是ipv6
     *
     * @param address 值
     * @return true 是，反之
     */
    public static boolean isIpv6StdAddress(final String address) {
        return address != null && IPV6_STD_PATTERN.matcher(address).matches();
    }

    /**
     * ipv6
     *
     * @param address
     * @return true是，反之
     */
    public static boolean isIpv6HexCompressedAddress(final String address) {
        return address != null && IPV6_HEX_COMPRESSED_PATTERN.matcher(address).matches();
    }

    /**
     * ipv6
     *
     * @param address
     * @return true 是，反之
     */
    public static boolean isIpv6Address(final String address) {
        return address != null && (isIpv6StdAddress(address) || isIpv6HexCompressedAddress(address));
    }

    /**
     * IPv4
     *
     * @return 第一个本地IPv4地址，或为空
     */
    public static InetAddress getLocalIpv4Address() {
        ArrayList<InetAddress> localAddresses = getLocalIpv4Addresses();
        return localAddresses.size() > 0 ? localAddresses.get(0) : null;
    }

    /**
     * IPv4地址的列表
     *
     * @return 找到的所有IPv4地址的列表
     */
    public static ArrayList<InetAddress> getLocalIpv4Addresses() {
        ArrayList<InetAddress> foundAddresses = new ArrayList<>();

        Enumeration<NetworkInterface> ifaces;
        try {
            ifaces = NetworkInterface.getNetworkInterfaces();

            while (ifaces.hasMoreElements()) {
                NetworkInterface iface = ifaces.nextElement();
                Enumeration<InetAddress> addresses = iface.getInetAddresses();
                while (addresses.hasMoreElements()) {
                    InetAddress addr = addresses.nextElement();
                    if (addr instanceof Inet4Address && !addr.isLoopbackAddress()) {
                        foundAddresses.add(addr);
                    }
                }
            }
        } catch (SocketException e) {
            e.printStackTrace();
        }
        return foundAddresses;
    }

    /**
     * 检查提供的ip地址是否指向本地主机
     *
     * @param addr -
     * @return -
     */
    public static boolean isIpAddressLocalhost(InetAddress addr) {
        if (addr == null) {
            return false;
        }
        if (addr.isAnyLocalAddress() || addr.isLoopbackAddress()) {
            return true;
        }
        try {
            return NetworkInterface.getByInetAddress(addr) != null;
        } catch (SocketException e) {
            return false;
        }
    }

    /**
     * 检查提供的ip地址是否指向本地主机
     *
     * @param addr -
     * @return -
     */
    public static boolean isIpAddressLocalNetwork(InetAddress addr) {
        return addr != null && addr.isSiteLocalAddress();
    }
}
