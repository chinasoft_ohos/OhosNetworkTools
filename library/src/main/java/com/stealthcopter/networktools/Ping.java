package com.stealthcopter.networktools;

import com.stealthcopter.networktools.ping.PingOptions;
import com.stealthcopter.networktools.ping.PingResult;
import com.stealthcopter.networktools.ping.PingStats;
import com.stealthcopter.networktools.ping.PingTools;

import java.math.BigDecimal;
import java.net.InetAddress;
import java.net.UnknownHostException;

public class Ping {
    /**
     * ping
     */
    public static final int PING_JAVA = 0;

    /**
     * ping
     */
    public static final int PING_NATIVE = 1;

    /**
     * ping
     */
    public static final int PING_HYBRID = 2;
    private String addressString = null;
    private InetAddress address;
    private final PingOptions pingOptions = new PingOptions();
    private int delayBetweenScansMillis = 0;
    private int times = 1;
    private boolean isCancelled = false;

    private Ping() {
    }

    public interface PingListener {
        /**
         * 成功
         *
         * @param pingResult
         */
        void onResult(PingResult pingResult);

        /**
         * 关闭
         *
         * @param pingStats
         */
        void onFinished(PingStats pingStats);

        /**
         * 错误
         *
         * @param e
         */
        void onError(Exception e);
    }

    /**
     * 地址设置为ping
     *
     * @param address - 需要ping通的地址
     * @return 此对象允许链接
     */
    public static Ping onAddress(String address) {
        Ping ping = new Ping();
        ping.setAddressString(address);
        return ping;
    }

    /**
     * 地址设置为ping
     *
     * @param ia - 需要ping通的地址
     * @return 此对象允许链接
     */
    public static Ping onAddress(InetAddress ia) {
        Ping ping = new Ping();
        ping.setAddress(ia);
        return ping;
    }

    /**
     * 设置超时
     *
     * @param timeOutMillis - 每次ping的超时时间(以毫秒为单位)
     * @return 此对象允许链接
     * @throws IllegalArgumentException 异常
     */
    public Ping setTimeOutMillis(int timeOutMillis) {
        if (timeOutMillis < 0) {
            throw new IllegalArgumentException("Times cannot be less than 0");
        }
        pingOptions.setTimeoutMillis(timeOutMillis);
        return this;
    }

    /**
     * 设置每次ping之间的延迟时间
     *
     * @param delayBetweenScansMillis - 每次ping的超时时间(以毫秒为单位)
     * @return 此对象允许链接
     * @throws IllegalArgumentException 异常
     */
    public Ping setDelayMillis(int delayBetweenScansMillis) {
        if (delayBetweenScansMillis < 0) {
            throw new IllegalArgumentException("Delay cannot be less than 0");
        }
        this.delayBetweenScansMillis = delayBetweenScansMillis;
        return this;
    }

    /**
     * 把时间设定为生活
     *
     * @param timeToLive - 每次ping的TTL值
     * @return 此对象允许链接
     * @throws IllegalArgumentException 异常
     */
    public Ping setTimeToLive(int timeToLive) {
        if (timeToLive < 1) {
            throw new IllegalArgumentException("TTL cannot be less than 1");
        }
        pingOptions.setTimeToLive(timeToLive);
        return this;
    }

    /**
     * 设置地址ping的次数
     *
     * @param noTimes - 次数，0 =连续
     * @return 链接
     * @throws IllegalArgumentException 异常
     */
    public Ping setTimes(int noTimes) {
        if (noTimes < 0) {
            throw new IllegalArgumentException("Times cannot be less than 0");
        }
        this.times = noTimes;
        return this;
    }

    private void setAddress(InetAddress address) {
        this.address = address;
    }

    private void setAddressString(String addressString) {
        this.addressString = addressString;
    }

    private void resolveAddressString() throws UnknownHostException {
        if (address == null && addressString != null) {
            address = InetAddress.getByName(addressString);
        }
    }

    /**
     * 取消运行ping
     */
    public void cancel() {
        this.isCancelled = true;
    }

    /**
     * 执行一次同步ping并返回结果，将忽略次数。
     *
     * @return -
     * @throws UnknownHostException - 主机无法解析
     */
    public PingResult doPing() throws UnknownHostException {
        isCancelled = false;
        resolveAddressString();
        return PingTools.doPing(address, pingOptions);
    }

    /**
     * 执行异步ping
     *
     * @param pingListener - 触发ping结果的监听器。
     * @return - 这样我们可以在需要的时候取消
     */
    public Ping doPing(final PingListener pingListener) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    resolveAddressString();
                } catch (UnknownHostException e) {
                    pingListener.onError(e);
                    return;
                }

                if (address == null) {
                    pingListener.onError(new NullPointerException("Address is null"));
                    return;
                }

                long pingsCompleted = 0;
                long noLostPackets = 0;
                float totalPingTime = 0;
                float minPingTime = -1;
                float maxPingTime = -1;

                isCancelled = false;
                int noPings = times;

                while (noPings > 0 || times == 0) {
                    PingResult pingResult = PingTools.doPing(address, pingOptions);

                    if (pingListener != null) {
                        pingListener.onResult(pingResult);
                    }

                    pingsCompleted++;
                    if (pingResult.hasError()) {
                        noLostPackets++;
                    } else {
                        float timeTaken = pingResult.getTimeTaken();
                        totalPingTime += timeTaken;
                        if (maxPingTime == -1 || timeTaken > maxPingTime) {
                            maxPingTime = timeTaken;
                        }
                        if (minPingTime == -1 || timeTaken < minPingTime) {
                            minPingTime = timeTaken;
                        }
                    }
                    noPings--;
                    if (isCancelled) {
                        break;
                    }

                    try {
                        Thread.sleep(delayBetweenScansMillis);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }

                if (pingListener != null) {
                    pingListener.onFinished(new PingStats(address, pingsCompleted, noLostPackets, totalPingTime, minPingTime, maxPingTime));
                }
            }
        }).start();
        return this;
    }
}