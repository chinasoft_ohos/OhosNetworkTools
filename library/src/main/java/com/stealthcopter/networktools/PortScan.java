package com.stealthcopter.networktools;

import com.stealthcopter.networktools.portscanning.PortScanTCP;
import com.stealthcopter.networktools.portscanning.PortScanUDP;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class PortScan {
    private static final int TIMEOUT_LOCALHOST = 25;
    private static final int TIMEOUT_LOCALNETWORK = 1000;
    private static final int TIMEOUT_REMOTE = 2500;

    private static final int DEFAULT_THREADS_LOCALHOST = 7;
    private static final int DEFAULT_THREADS_LOCALNETWORK = 50;
    private static final int DEFAULT_THREADS_REMOTE = 50;

    private static final int METHOD_TCP = 0;
    private static final int METHOD_UDP = 1;

    private int method = METHOD_TCP;
    private int noThreads = 50;
    private InetAddress address;
    private int timeOutMillis = 1000;
    private boolean isCancelled = false;

    private ArrayList<Integer> ports = new ArrayList<>();
    private ArrayList<Integer> openPortsFound = new ArrayList<>();

    private PortListener portListener;

    // 这个类不会被实例化
    private PortScan() {
    }

    public interface PortListener {
        /**
         * 成功
         *
         * @param portNo port
         * @param isOpen 是否打开
         */
        void onResult(int portNo, boolean isOpen);

        /**
         * 关闭
         *
         * @param openPorts
         */
        void onFinished(ArrayList<Integer> openPorts);
    }

    /**
     * 地址设置为ping
     *
     * @param address - 需要ping通的地址
     * @return 此对象允许链接
     * @throws UnknownHostException - 如果没有IP地址可以找到，或者如果指定了scope_id表示全局IPv6地址
     */
    public static PortScan onAddress(String address) throws UnknownHostException {
        return onAddress(InetAddress.getByName(address));
    }

    /**
     * 地址设置为ping
     *
     * @param ia - 需要ping通的地址
     * @return 此对象允许链接
     */
    public static PortScan onAddress(InetAddress ia) {
        PortScan portScan = new PortScan();
        portScan.setAddress(ia);
        portScan.setDefaultThreadsAndTimeouts();
        return portScan;
    }

    /**
     * 设置每个扫描端口的超时时间如果您提高了超时时间，您可能需要考虑增加线程计数
     *
     * @param timeOutMillis - 每次ping的超时时间(以毫秒为单位)
     * @return 此对象允许链接
     */
    public PortScan setTimeOutMillis(int timeOutMillis) {
        if (timeOutMillis < 0) throw new IllegalArgumentException("Timeout cannot be less than 0");
        this.timeOutMillis = timeOutMillis;
        return this;
    }

    /**
     * 扫描需要扫描的端口
     *
     * @param port - 需要扫描的端口
     * @return 此对象允许链接
     */
    public PortScan setPort(int port) {
        ports.clear();
        validatePort(port);
        ports.add(port);
        return this;
    }

    /**
     * 扫描需要扫描的端口
     *
     * @param ports - 需要扫描的端口
     * @return 此对象允许链接
     */
    public PortScan setPorts(ArrayList<Integer> ports) {
        // Check all ports are valid
        for (Integer port : ports) {
            validatePort(port);
        }

        this.ports = ports;

        return this;
    }

    /**
     * 扫描需要扫描的端口
     *
     * @param portString - 要扫描的端口(逗号分隔，连字符表示范围)
     * @return 此对象允许链接
     */
    public PortScan setPorts(String portString) {
        ports.clear();

        ArrayList<Integer> ports = new ArrayList<>();

        if (portString == null) {
            throw new IllegalArgumentException("Empty port string not allowed");
        }

        portString = portString.substring(portString.indexOf(":") + 1, portString.length());

        for (String x : portString.split(",")) {
            if (x.contains("-")) {
                int start = Integer.parseInt(x.split("-")[0]);
                int end = Integer.parseInt(x.split("-")[1]);
                validatePort(start);
                validatePort(end);
                if (end <= start) {
                    throw new IllegalArgumentException("Start port cannot be greater than or equal to the end port");
                }
                for (int j = start; j <= end; j++) {
                    ports.add(j);
                }
            } else {
                int start = Integer.parseInt(x);
                validatePort(start);
                ports.add(start);
            }
        }

        this.ports = ports;

        return this;
    }

    /**
     * 如果端口无效，则检查并抛出异常
     *
     * @param port - 要验证的端口
     */
    private void validatePort(int port) {
        if (port < 1) {
            throw new IllegalArgumentException("Start port cannot be less than 1");
        }
        if (port > 65535) {
            throw new IllegalArgumentException("Start cannot be greater than 65535");
        }
    }

    /**
     * 扫描所有特权端口
     *
     * @return 此对象允许链接
     */
    public PortScan setPortsPrivileged() {
        ports.clear();
        for (int i = 1; i < 1024; i++) {
            ports.add(i);
        }
        return this;
    }

    /**
     * 扫描所有端口
     *
     * @return 此对象允许链接
     */
    public PortScan setPortsAll() {
        ports.clear();
        for (int i = 1; i < 65536; i++) {
            ports.add(i);
        }
        return this;
    }

    private void setAddress(InetAddress address) {
        this.address = address;
    }

    private void setDefaultThreadsAndTimeouts() {
        // 试着自动找出我们正在扫描的是哪种主机
        // 本地主机(此设备)/本地网络/远程
        if (IPTools.isIpAddressLocalhost(address)) {
            // 如果我们正在扫描a，则将本地主机的超时设置为非常短，这样我们可以得到更快的结果
            // 如果用户手动调用setTimeoutMillis，这将被覆盖。
            timeOutMillis = TIMEOUT_LOCALHOST;
            noThreads = DEFAULT_THREADS_LOCALHOST;
        } else if (IPTools.isIpAddressLocalNetwork(address)) {
            // 假设本地网络(并非绝对可靠)
            timeOutMillis = TIMEOUT_LOCALNETWORK;
            noThreads = DEFAULT_THREADS_LOCALNETWORK;
        } else {
            // 假设远程网络超时
            timeOutMillis = TIMEOUT_REMOTE;
            noThreads = DEFAULT_THREADS_REMOTE;
        }
    }

    /**
     * 设置要处理的线程数
     *
     * @param noThreads 设置要处理的线程数，注意我们默认为一个较大的数字因为这些请求是网络繁忙而不是cpu繁忙。
     * @return 自己
     * @throws IllegalArgumentException - 如果没有线程小于1
     */
    public PortScan setNoThreads(int noThreads) throws IllegalArgumentException {
        if (noThreads < 1) {
            throw new IllegalArgumentException("Cannot have less than 1 thread");
        }
        this.noThreads = noThreads;
        return this;
    }

    private PortScan setMethod(int method) {
        switch (method) {
            case METHOD_UDP:
            case METHOD_TCP:
                this.method = method;
                break;
            default:
                throw new IllegalArgumentException("Invalid method type " + method);
        }
        return this;
    }

    /**
     * 设置“扫描方式”为“UDP”
     *
     * @return 此对象允许链接
     */
    public PortScan setMethodUdP() {
        setMethod(METHOD_UDP);
        return this;
    }

    /**
     * 设置“扫描方式”为“TCP”
     *
     * @return 此对象允许链接
     */
    public PortScan setMethodTcP() {
        setMethod(METHOD_TCP);
        return this;
    }

    /**
     * 取消
     */
    public void cancel() {
        this.isCancelled = true;
    }

    /**
     * 执行同步(阻塞)端口扫描并返回开放端口列表
     *
     * @return -结果
     */
    public ArrayList<Integer> doScan() {
        isCancelled = false;
        openPortsFound.clear();

        ExecutorService executor = Executors.newFixedThreadPool(noThreads);

        for (int portNo : ports) {
            Runnable worker = new PortScanRunnable(address, portNo, timeOutMillis, method);
            executor.execute(worker);
        }

        // 这将使执行程序不接受新线程
        // 并完成队列中的所有现有线程
        executor.shutdown();
        // 等待所有线程完成
        try {
            executor.awaitTermination(1, TimeUnit.HOURS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        Collections.sort(openPortsFound);

        return openPortsFound;
    }

    /**
     * 执行异步(非阻塞)端口扫描
     *
     * @param portListener - 触发portscan结果的监听器。
     * @return - 这样我们就可以在需要的时候取消扫描
     */
    public synchronized PortScan doScan(final PortListener portListener) {
        this.portListener = portListener;
        openPortsFound.clear();
        isCancelled = false;

        new Thread(new Runnable() {
            @Override
            public void run() {
                ExecutorService executor = Executors.newFixedThreadPool(noThreads);

                for (int portNo : ports) {
                    Runnable worker = new PortScanRunnable(address, portNo, timeOutMillis, method);
                    executor.execute(worker);
                }
                executor.shutdown();
                try {
                    executor.awaitTermination(1, TimeUnit.HOURS);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                if (portListener != null) {
                    Collections.sort(openPortsFound);
                    portListener.onFinished(openPortsFound);
                }
            }
        }).start();

        return this;
    }

    private synchronized void portScanned(int port, boolean isOpen) {
        if (isOpen) {
            openPortsFound.add(port);
        }
        if (portListener != null) {
            portListener.onResult(port, isOpen);
        }
    }

    private class PortScanRunnable implements Runnable {
        private final InetAddress address;
        private final int portNo;
        private final int timeOutMillis;
        private final int method;

        PortScanRunnable(InetAddress address, int portNo, int timeOutMillis, int method) {
            this.address = address;
            this.portNo = portNo;
            this.timeOutMillis = timeOutMillis;
            this.method = method;
        }

        @Override
        public void run() {
            if (isCancelled) {
                return;
            }
            switch (method) {
                case METHOD_UDP:
                    portScanned(portNo, PortScanUDP.scanAddress(address, portNo, timeOutMillis));
                    break;
                case METHOD_TCP:
                    portScanned(portNo, PortScanTCP.scanAddress(address, portNo, timeOutMillis));
                    break;
                default:
                    throw new IllegalArgumentException("Invalid method");
            }
        }
    }
}