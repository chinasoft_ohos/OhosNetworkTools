package com.stealthcopter.networktools;

import com.stealthcopter.networktools.ping.PingResult;
import com.stealthcopter.networktools.subnet.Device;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class SubnetDevices {
    private int noThreads = 100;

    private ArrayList<String> addresses;
    private ArrayList<Device> devicesFound;
    private OnSubnetDeviceFound listener;
    private int timeOutMillis = 2500;
    private boolean isCancelled = false;

    private boolean isDisableProcNetMethod = false;
    private HashMap<String, String> ipMacHashMap = null;

    // 这个类不会被实例化
    private SubnetDevices() {
    }

    public interface OnSubnetDeviceFound {
        /**
         * 开始
         *
         * @param device 值
         */
        void onDeviceFound(Device device);

        /**
         * 结束
         *
         * @param devicesFound 集合
         */
        void onFinished(ArrayList<Device> devicesFound);
    }

    /**
     * 从本端设备的ip地址查找子网中工作的设备
     *
     * @return - 链接
     */
    public static SubnetDevices fromLocalAddress() {
        InetAddress ipv4 = IPTools.getLocalIpv4Address();

        if (ipv4 == null) {
            throw new IllegalAccessError("Could not access local ip address");
        }

        return fromIpAddress(ipv4.getHostAddress());
    }

    /**
     * ip
     *
     * @param inetAddress - 子网中的ip地址
     * @return - 链接
     */
    public static SubnetDevices fromIpAddress(InetAddress inetAddress) {
        return fromIpAddress(inetAddress.getHostAddress());
    }

    /**
     * ip地址
     *
     * @param ipAddress - 该子网中任何设备的ipAddress字符串。“192.168.0.1”最后一部分将被忽略
     * @return - 链接
     * @throws IllegalArgumentException 异常
     */
    public static SubnetDevices fromIpAddress(final String ipAddress) {
        if (!IPTools.isIpv4Address(ipAddress)) {
            throw new IllegalArgumentException("Invalid IP Address");
        }

        String segment = ipAddress.substring(0, ipAddress.lastIndexOf(".") + 1);

        SubnetDevices subnetDevice = new SubnetDevices();

        subnetDevice.addresses = new ArrayList<>();

        // 首先从ARP信息中获取地址，因为它们可能是可访问的
        for (String ip : ARPInfo.getAllIPAddressesInARPCache()) {
            if (ip.startsWith(segment)) {
                subnetDevice.addresses.add(ip);
            }
        }

        // 添加子网中所有丢失的地址
        for (int j = 0; j < 255; j++) {
            if (!subnetDevice.addresses.contains(segment + j)) {
                subnetDevice.addresses.add(segment + j);
            }
        }
        return subnetDevice;
    }

    /**
     * ip
     *
     * @param ipAddresses - 待检查设备的ip地址
     * @return - 链接
     */
    public static SubnetDevices fromIpList(final List<String> ipAddresses) {
        SubnetDevices subnetDevice = new SubnetDevices();
        subnetDevice.addresses = new ArrayList<>();

        subnetDevice.addresses.addAll(ipAddresses);

        return subnetDevice;
    }

    /**
     * 连接
     *
     * @param noThreads 设置要处理的线程数，注意我们默认为一个较大的数字由于这些请求是网络繁忙，而不是cpu繁忙。
     * @return - 连接
     * @throws IllegalArgumentException - 如果请求的线程数无效
     */
    public SubnetDevices setNoThreads(int noThreads) throws IllegalArgumentException {
        if (noThreads < 1) {
            throw new IllegalArgumentException("Cannot have less than 1 thread");
        }
        this.noThreads = noThreads;
        return this;
    }

    /**
     * 为我们尝试ping的每个地址设置超时
     *
     * @param timeOutMillis - 每次ping的超时时间(毫秒)
     * @return 此对象允许链接
     * @throws IllegalArgumentException - 如果timeout小于0
     */
    public SubnetDevices setTimeOutMillis(int timeOutMillis) throws IllegalArgumentException {
        if (timeOutMillis < 0) {
            throw new IllegalArgumentException("Timeout cannot be less than 0");
        }
        this.timeOutMillis = timeOutMillis;
        return this;
    }

    /**
     * 日志
     *
     * @param isDisable 如果设置为true，我们将不会尝试读取/proc/net/arp直接。这样可以避免出现任何 10权限日志。
     */
    public void setDisableProcNetMethod(boolean isDisable) {
        this.isDisableProcNetMethod = isDisable;
    }

    /**
     * 取消正在运行的扫描
     */
    public void cancel() {
        this.isCancelled = true;
    }

    /**
     * 开始扫描以发现子网中的其他设备
     *
     * @param listener - 传递结果
     * @return 这个对象，如果需要，我们可以调用cancel
     */
    public synchronized SubnetDevices findDevices(final OnSubnetDeviceFound listener) {
        this.listener = listener;

        isCancelled = false;
        devicesFound = new ArrayList<>();

        new Thread(new Runnable() {
            @Override
            public void run() {
                // 将mac地址加载到缓存变量中(以避免/proc/net/arp文件在
                // 网络上有很多设备
                ipMacHashMap = isDisableProcNetMethod ? ARPInfo.getAllIPandMACAddressesFromIPSleigh() : ARPInfo.getAllIPAndMACAddressesInARPCache();

                ExecutorService executor = Executors.newFixedThreadPool(noThreads);

                for (final String add : addresses) {
                    Runnable worker = new SubnetDeviceFinderRunnable(add);
                    executor.execute(worker);
                }

                // 这将使执行程序不接受新线程
                // 并完成队列中的所有现有线程

                executor.shutdown();

                // 等待所有线程完成
                try {
                    executor.awaitTermination(1, TimeUnit.HOURS);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                // 循环查找到的设备，如果缺少MAC地址，则添加MAC地址。
                // 我们在扫描所有设备后这样做，因为/proc/net/arp可能会添加信息
                // 因为扫描的原因
                ipMacHashMap = isDisableProcNetMethod ? ARPInfo.getAllIPandMACAddressesFromIPSleigh() : ARPInfo.getAllIPAndMACAddressesInARPCache();
                for (Device device : devicesFound) {
                    if (device.mac == null && ipMacHashMap.containsKey(device.ip)) {
                        device.mac = ipMacHashMap.get(device.ip);
                    }
                }
                listener.onFinished(devicesFound);
            }
        }).start();

        return this;
    }

    private synchronized void subnetDeviceFound(Device device) {
        devicesFound.add(device);
        listener.onDeviceFound(device);
    }

    /**
     * 线程
     */
    public class SubnetDeviceFinderRunnable implements Runnable {
        private final String address;

        SubnetDeviceFinderRunnable(String address) {
            this.address = address;
        }

        @Override
        public void run() {
            if (isCancelled) {
                return;
            }

            try {
                InetAddress ia = InetAddress.getByName(address);
                PingResult pingResult = Ping.onAddress(ia).setTimeOutMillis(timeOutMillis).doPing();
                if (pingResult.isReachable) {
                    Device device = new Device(ia);

                    // 如果设备MAC地址在缓存中，请添加该设备的MAC地址
                    if (ipMacHashMap.containsKey(ia.getHostAddress())) {
                        device.mac = ipMacHashMap.get(ia.getHostAddress());
                    }

                    device.time = pingResult.timeTaken;
                    subnetDeviceFound(device);
                }
            } catch (UnknownHostException e) {
                e.printStackTrace();
            }
        }
    }
}