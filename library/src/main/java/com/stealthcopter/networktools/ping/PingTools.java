package com.stealthcopter.networktools.ping;

import java.io.IOException;
import java.net.InetAddress;

public class PingTools {
    private PingTools() {
    }

    /**
     * 使用本机ping工具执行ping，然后退回到使用java echo请求在失败
     *
     * @param ia - ping
     * @param pingOptions - ping
     * @return - ping
     */
    public static PingResult doPing(InetAddress ia, PingOptions pingOptions) {
        try {
            return PingTools.doNativePing(ia, pingOptions);
        } catch (InterruptedException e) {
            PingResult pingResult = new PingResult(ia);
            pingResult.isReachable = false;
            pingResult.error = "Interrupted";
            return pingResult;
        } catch (Exception ignored) {
        }
        return PingTools.doJavaPing(ia, pingOptions);
    }

    /**
     * 使用本机ping二进制文件执行ping
     *
     * @param ia - ping
     * @param pingOptions - ping
     * @return - ping
     * @throws IOException - ping
     * @throws InterruptedException - thread interrupt
     */

    public static PingResult doNativePing(InetAddress ia, PingOptions pingOptions) throws IOException, InterruptedException {
        return PingNative.ping(ia, pingOptions);
    }

    public static PingResult doJavaPing(InetAddress ia, PingOptions pingOptions) {
        PingResult pingResult = new PingResult(ia);
        if (ia == null) {
            pingResult.isReachable = false;
            return pingResult;
        }
        try {
            long startTime = System.nanoTime();
            final boolean isReached = ia.isReachable(null, pingOptions.getTimeToLive(), pingOptions.getTimeoutMillis());
            pingResult.timeTaken = (System.nanoTime() - startTime) / 1e6f;
            pingResult.isReachable = isReached;
            if (!isReached) {
                pingResult.error = "Timed Out";
            }
        } catch (IOException e) {
            pingResult.isReachable = false;
            pingResult.error = "IOException: " + e.getMessage();
        }
        return pingResult;
    }
}
