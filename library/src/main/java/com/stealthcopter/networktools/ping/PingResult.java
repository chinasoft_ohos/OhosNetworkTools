package com.stealthcopter.networktools.ping;

import java.net.InetAddress;

public class PingResult {
    /**
     * 地址
     */
    public final InetAddress ia;

    /**
     * 是否获取
     */
    public boolean isReachable;

    /**
     * 错误
     */
    public String error = null;

    /**
     * 时间
     */
    public float timeTaken;
    /**
     * string类型
     */
    public String fullString;
    /**
     * 重构
     */
    public String result;

    /**
     * 构造
     *
     * @param ia id地址
     */
    public PingResult(InetAddress ia) {
        this.ia = ia;
    }

    public boolean isReachable() {
        return isReachable;
    }

    /**
     * 布尔
     *
     * @return 是否
     */
    public boolean hasError() {
        return error != null;
    }

    public float getTimeTaken() {
        return timeTaken;
    }

    public String getError() {
        return error;
    }

    public InetAddress getAddress() {
        return ia;
    }

    @Override
    public String toString() {
        return "PingResult{"
                + "ia=" + ia
                + ", isReachable=" + isReachable
                + ", error='" + error + '\''
                + ", timeTaken=" + timeTaken
                + ", fullString='" + fullString + '\''
                + ", result='" + result + '\''
                + '}';
    }
}
