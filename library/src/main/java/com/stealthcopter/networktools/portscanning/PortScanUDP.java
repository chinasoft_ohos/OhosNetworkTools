package com.stealthcopter.networktools.portscanning;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketTimeoutException;

public class PortScanUDP {
    private PortScanUDP() {
    }

    /**
     * 检查端口是否使用UDP打开，注意这是不可靠的
     *
     * @param ia 地址扫描
     * @param portNo - port
     * @param timeoutMillis - time
     * @return - 如果端口是开放的，则为true，否则为false或未知
     */
    public static boolean scanAddress(InetAddress ia, int portNo, int timeoutMillis) {
        try {
            byte[] bytes = new byte[128];
            DatagramPacket dp = new DatagramPacket(bytes, bytes.length);
            DatagramSocket ds = new DatagramSocket();
            ds.setSoTimeout(timeoutMillis);
            ds.connect(ia, portNo);
            ds.send(dp);
            ds.isConnected();
            ds.receive(dp);
            ds.close();
        } catch (SocketTimeoutException e) {
            return true;
        } catch (Exception ignore) {
        }
        return false;
    }
}
