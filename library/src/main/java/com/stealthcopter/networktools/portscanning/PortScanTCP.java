package com.stealthcopter.networktools.portscanning;

import javax.net.ssl.HandshakeCompletedListener;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocket;
import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;

public class PortScanTCP {
    private PortScanTCP() {
    }

    /**
     * 检查一个端口是否用TCP打开
     *
     * @param ia -
     * @param portNo - port
     * @param timeoutMillis -tiem
     * @return -如果端口是开放的，则为true，否则为false或未知
     */
    public static boolean scanAddress(InetAddress ia, int portNo, int timeoutMillis) {
        SSLSocket s = null;
        try {
            s = new SSLSocket() {
                @Override
                public String[] getSupportedCipherSuites() {
                    return new String[0];
                }

                @Override
                public String[] getEnabledCipherSuites() {
                    return new String[0];
                }

                @Override
                public void setEnabledCipherSuites(String[] strings) {
                }

                @Override
                public String[] getSupportedProtocols() {
                    return new String[0];
                }

                @Override
                public String[] getEnabledProtocols() {
                    return new String[0];
                }

                @Override
                public void setEnabledProtocols(String[] strings) {
                }

                @Override
                public SSLSession getSession() {
                    return null;
                }

                @Override
                public void addHandshakeCompletedListener(HandshakeCompletedListener handshakeCompletedListener) {
                }

                @Override
                public void removeHandshakeCompletedListener(HandshakeCompletedListener handshakeCompletedListener) {
                }

                @Override
                public void startHandshake() throws IOException {
                }

                @Override
                public void setUseClientMode(boolean isClient) {
                }

                @Override
                public boolean getUseClientMode() {
                    return false;
                }

                @Override
                public void setNeedClientAuth(boolean isNeed) {
                }

                @Override
                public boolean getNeedClientAuth() {
                    return false;
                }

                @Override
                public void setWantClientAuth(boolean isWant) {
                }

                @Override
                public boolean getWantClientAuth() {
                    return false;
                }

                @Override
                public void setEnableSessionCreation(boolean isEnable) {
                }

                @Override
                public boolean getEnableSessionCreation() {
                    return false;
                }
            };
            s.connect(new InetSocketAddress(ia, portNo), timeoutMillis);
            return true;
        } catch (IOException e) {
        } finally {
            if (s != null) {
                try {
                    s.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return false;
    }
}
